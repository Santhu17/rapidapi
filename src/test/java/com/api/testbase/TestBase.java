package com.api.testbase;

import org.junit.BeforeClass;

import io.restassured.RestAssured;

public class TestBase {
	@BeforeClass
	public static void init() {
		RestAssured.baseURI="https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com";
	}
}
