package com.api.cucumber.steps;

import com.api.cucumber.serenity.APISteps;
import com.api.utils.ReusableSpecs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

public class RapidAPISteps {

	@Steps
	APISteps steps;
	
	ValidatableResponse parsedIngredients;
	ValidatableResponse detectFoodInText;

	@When("^I Validate Analyze a Recipe Search with Query salmon with fusilli$")
	public void requestRecipe() {
		SerenityRest.rest()
		.given().spec(ReusableSpecs.getGenericRequestSpec())
		.when().get("/recipes/queries/analyze?q=salmon%20with%20fusilli%20")
		.then().statusCode(200).extract().path("ingredients[1].name");
		assertThat(detectFoodInText.statusCode(200).extract().path("parsedInstructions[0].steps[0].ingredients[0].name"), equalTo(""));
	}
	
	@When("^I parse ingredients (.*) with servings (.*)$")
	public void i_parse_ingredients(String ingrentient, String servings){
		parsedIngredients = steps.parsingIngredientsFromString(ingrentient, servings);
	}
	
	@Then("^I validate status code (.*) and the post response containing (.*) in the Parsed Ingredients$")
	public void i_validate_status_code_and_the_post_response_containing_in_the_Parsed_Ingredients(int statusCode, String ExpectedResponse) {
		   assertThat(parsedIngredients.statusCode(statusCode).extract().path("[0].originalName"), equalTo(ExpectedResponse));
	}
	
	@When("^I test POST Request to Detect Food in Text with text content (.*)$")
	public void i_test_POST_Request_Detect_Food_in_Text_with_text_content_I_like_to_eat_delicious_tacos(String inputText) {
		detectFoodInText = steps.detectFoodInText(inputText);
	}

	@Then("^I validate status code (.*) and the post response containing annotation (.*) from the Detect food in Text$")
	public void i_validate_status_code_and_the_post_response_containing_annotation_from_the_Detect_food_in_Text(int statusCode, String annotation) {
		   assertThat(detectFoodInText.statusCode(statusCode).extract().path("annotations[0].annotation"), equalTo(annotation));
	}
	
	@When("^I test POST Request to Analyse Recipe Instrucitons (.*)$")
	public void i_test_POST_Request_analyse_recipe_instructions(String instructions) {
		detectFoodInText = steps.analyseRecipeInstructions(instructions);
	}
	
	@Then("^I validate status code (.*) and the post response containing name (.*) from the Analyze Recipe Instructions$")
	public void i_validate_status_code_and_the_post_response_containing_annotation_from_the_analyze_recipe_instructions(int statusCode, String annotation) {
		   assertThat(detectFoodInText.statusCode(statusCode).extract().path("parsedInstructions[0].steps[0].ingredients[0].name"), equalTo(annotation));
	}
	

}
