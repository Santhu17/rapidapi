Feature: Testing different requests of RapidAPI

Scenario Outline: Validate the Parse Ingredeients POST Request and response
When I parse ingredients <Ingredients> with servings <servings> 
Then I validate status code <status_code> and the post response containing <response_validation> in the Parsed Ingredients

Examples:
	| Ingredients 		   | servings | status_code | response_validation   | 
	| 3 oz chicken breast  | 4 		  | 200 		| chicken breast    	| 
	| 1 kg pork legs       | 4 		  | 200 		| pork legs          	| 


Scenario Outline: Validate the Detect Food in Text POST Request
When I test POST Request to Detect Food in Text with text content <TextContent> 
Then I validate status code <status_code> and the post response containing annotation <response_validation> from the Detect food in Text 

Examples:
	| TextContent 		                      	| response_validation   	| status_code |
	| I like to eat delicious tacos.         	| tacos    					| 200		  |
	| Only cheeseburger is enough for lunch!    | cheeseburger          	| 200		  |
	| Wow tomatoes is so good!					| tomatoes					| 200		  |


Scenario Outline: Validate the Analyse Recipe Instrucitons
When I test POST Request to Analyse Recipe Instrucitons <Instructions>
Then I validate status code <status_code> and the post response containing name <name> from the Analyze Recipe Instructions

Examples:
	| Instructions 		| name			| status_code |
	| garlic chicken	| whole chicken | 200		  |

