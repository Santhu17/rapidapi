# Rapidapi

Rapid API automation
Project used : https://rapidapi.com/spoonacular/api/recipe-food-nutrition/details 

# Endpoints: 
"https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/parseIngredients" - ParseIngredients API

"https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/detect - Detect Food in Text API

"https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/analyzeInstructions" - Analyze Recipe Instructions API

# Project Setup
## Tools Required/used
1. Java Jdk 1.8 installed on the system
2. maven 
3. Eclipse IDE 
4. GIT installed on the system
5. sourcetree to manage git push pull (optional)

To setup the project on the local system 
1. checkout the code repository to your local system by cloning "git@gitlab.com:Santhu17/rapidapi.git project"
2. Open eclipse and import the porject as a maven project 
3. After importing the porject please wait until the maven downloads all the dependencies on to local system from maven repo (this could take approx 2 to 5 min - depending on existing repositories and internet speed)

# Creating Run Configuration :

1. In the eclipse navigate to Run menu -> Run Configurations - >Select Maven Build (double click) this would open a menu to create a new run config please enter the below details to the menu for the run configuration . 

a. in the base directory select the workspace as the project 

b. In the goals enter - clean verify serenity:aggregate (or) clean verify serenity:aggregate -Dmaven.surefire.debug test

c. in the maven runtime i recommend to use embedded version of maven as default (optional)

d. Click on the JRE Tab. And ensure to select alternate jre (which has jdk in it V-1.8 preferred)

![image-2.png](./image-2.png)

e. forgot to mention at the top . we need to give a run configuration name for that we can give any name 

![image-1.png](./image-1.png)

f. Once the above steps are completed click on apply and run 

This would execute the test scripts . 

# Reading the Result
1. once the execution is completed. In the project there will be a folder containing the report (refresh the project to reveal that folder) navigate to target -> site -> serenity -> index.html 

![image-3.png](./image-3.png)

2. This would open the report we can navigate in the report to see what all test cases has been executed and if we drill down a bit we can see the request and response of the  API's 

![image-4.png](./image-4.png)

![image-5.png](./image-5.png)



# NOTE 1:
The rapid api allows only 50 requests per day (per rapidapi-key in free plan) so while executing the test scripts if it fails please switch the rapidapi-key 

To switch the rapidapi key -> go to com.api.utils package and go to ReusableSpecs.java file and if we go to line 24 and 25 there are 2 keys if one doesent work please comment that and uncomment the other while execution.

![image.png](./image.png)


# FOR ANY ERRORS/CLARIFICATIONS PLEASE FEEL FREE TO REACH OUT TO ME ON santhosh.s17@live.com or +49-15213881019 (Available on whatsapp) or https://www.linkedin.com/in/santhoshsid/ 

