package com.api.cucumber.serenity;

import java.util.List;

import com.api.utils.ReusableSpecs;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class APISteps {
	@Step("Testing the Parse Ingredients from Ingredients List")
	public ValidatableResponse parsingIngredientsFromString(String ingrentient, String servings) {
		return SerenityRest.rest()
				.given().spec(ReusableSpecs.getGenericRequestSpec())
			    .when().body("ingredientList="+ingrentient+"&servings="+servings+"").post("/recipes/parseIngredients")
			    .then();
	}
	@Step("Testing the Detect Food in Text Request")
	public ValidatableResponse detectFoodInText(String inputText) {
		return SerenityRest.rest()
				.given().spec(ReusableSpecs.getGenericRequestSpec())
			    .when().body("text="+inputText).post("/food/detect")
			    .then();
	}
	@Step("Testing the Analyse Recipe Instructions Request")
	public ValidatableResponse analyseRecipeInstructions(String instructions) {
		return SerenityRest.rest()
				.given().spec(ReusableSpecs.getGenericRequestSpec())
			    .when().body("instructions="+instructions).post("/recipes/analyzeInstructions")
			    .then();
	}
}
