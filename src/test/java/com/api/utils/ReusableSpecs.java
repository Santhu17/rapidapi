package com.api.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import static org.hamcrest.Matchers.*;

import java.util.concurrent.TimeUnit;

public class ReusableSpecs {
	
	public static RequestSpecBuilder reqspec;
	public static RequestSpecification requestSpecification;
	
	public static ResponseSpecBuilder respspec;
	public static ResponseSpecification responseSpecification;
	
	
	public static RequestSpecification getGenericRequestSpec() {
		reqspec = new RequestSpecBuilder();
		reqspec.setContentType(ContentType.JSON);
		reqspec.addHeader("x-rapidapi-key","b989126c17msh6dd494e6c360541p155333jsn4077952885cb");
//		reqspec.addHeader("x-rapidapi-key","1abde0ad4fmsh514fa327a31f72fp197ec7jsn3bfe935a51bf");
		reqspec.addHeader("x-rapidapi-host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
		reqspec.addHeader("content-type", "application/x-www-form-urlencoded");
//		reqspec.addHeader("useQueryString", "true");
		reqspec.setUrlEncodingEnabled(false);
		requestSpecification = reqspec.build();
		return requestSpecification;
	}
	
	public static ResponseSpecification getGenericResponseSpec() {
		respspec = new ResponseSpecBuilder();
		respspec.expectHeader("Content-Type", "application/json;charset=UTF-8");
		respspec.expectHeader("Transfer-Encoding", "chunked");
		respspec.expectResponseTime(lessThan(5L),TimeUnit.SECONDS);
		responseSpecification = respspec.build();
		return responseSpecification;
		
	}
}
